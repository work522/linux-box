// 1. Test

function print(str) {
  const firstString = str;
  return (str) => {
    return `${firstString} ${str}`;
  };
}

console.log(print("Hello")("World"));
console.log(print("Good")("Luck"));

// 2. Test

const a = 5;
const b = 10;
const info = `Soucet cisla ${a} a ${b} je: `;

const sum = (a, b) => {
  return Number(a + b);
};

let myPromise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(sum(a, b));
  }, 1000);
});

myPromise.then((message) => {
  console.log(`${info}` + message);
});

console.log(myPromise);
